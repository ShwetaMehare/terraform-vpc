# terraform {
#   required_providers {
#       aws = {
#         source  = "hashicorp/aws"
#         version = "5.40.0"
#       }
#     }
# }

# provider "aws" {
#   region = "us-east-2"
#   shared_config_files = [ "/root/.aws/config" ]
#   shared_credentials_files = [ "/root/.aws/credentials" ]
# }

# resource "aws_lb" "test" {
#   name               = "test-lb-tf"
#   internal           = false
#   load_balancer_type = "application"
#   security_groups    = ["sg-074020eb7754a2e72"]
#   subnets            = ["subnet-002842b894812add1","subnet-041dfb4f5926800f0"]
#   enable_deletion_protection = true

#   access_logs {
#     bucket  = "shweta-15"
#     prefix  = "test-lb"
#     enabled = true
#   }

#   tags = {
#     Environment = "production"
#   }
# }

# resource "aws_lb" "test-1" {
#   name               = "test-1-lb-tf"
#   internal           = false
#   load_balancer_type = "network"
#   subnets            = ["subnet-002842b894812add1","subnet-041dfb4f5926800f0"]

#   enable_deletion_protection = true

#   tags = {
#     Environment = "production"
#   }
# }

# resource "aws_lb" "test2" {
#   name               = "test2-lb-tf"
#   load_balancer_type = "network"

#   subnet_mapping {
#     subnet_id     = "subnet-002842b894812add1"
#     allocation_id = "eipalloc-01c7d3fc4a0938dc6"
#   }
# }