variable "region" {
    default = "us-west-1"
    description = "providing region for my server"
  
}

variable "ami" {
    default= "ami-05c969369880fa2c2"
    description = "providing image id for my server creation"
}

variable "instance_type" {
    default = "t2.micro"
    description = "providing the server size to my instace"
  
}

variable "key_name" {
    default = "pair"
    description = "providing key pair to my server"  
}